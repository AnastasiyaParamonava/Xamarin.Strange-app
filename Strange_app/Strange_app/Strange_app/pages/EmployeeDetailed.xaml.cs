﻿using Strange_app.dto;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Strange_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EmployeeDetailed : ContentPage
    {
        public Employee Employee { get; set; }

        public Command EditEmployeeCommand { get; set; }

        public EmployeeDetailed()
        {
            InitializeComponent();
            EditEmployeeCommand = new Command<Employee>(EditEmployee);
        }

        public EmployeeDetailed(Employee employee) : this()
        {
            Employee = employee;
            this.BindingContext = this;
        }

        public async void EditEmployee(Employee employee)
        {
            if (Employee != null)
                await Navigation.PushAsync(new EmployeeDetailedEdit(Employee));
        }
    }
}