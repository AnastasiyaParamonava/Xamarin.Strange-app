﻿using Strange_app.dto;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Strange_app
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PictureList : ContentPage
    {
        public List<Picture> Pictures { get; set; }

        public PictureList()
        {
            InitializeComponent();

            Pictures = App.Pictures;
            
            this.BindingContext = this;
        }

        public async void OnPictureTapped(object sender, ItemTappedEventArgs e)
        {
            
            var selectedPicture = e.Item as Picture;
            if (selectedPicture != null)
            {
                NavigationPage navigationPage = (NavigationPage)Application.Current.MainPage;
                IReadOnlyList<Page> navStack = navigationPage.Navigation.NavigationStack;
                var editPage = navStack[navigationPage.Navigation.NavigationStack.Count - 2] as EmployeeDetailedEdit;

                if (editPage != null)
                {
                    editPage.SetPicture(selectedPicture);
                }

                await Navigation.PopAsync();

            }
        }
    }
}