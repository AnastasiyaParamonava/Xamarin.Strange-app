﻿using Strange_app.dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Strange_app
{
    public partial class App : Application
    {

        public static List<Picture> Pictures = new List<Picture>()
            {
                new Picture{Name = "icon1.jpg" },
                new Picture{Name = "icon2.jpg" },
                new Picture{Name = "icon3.jpg" },
                new Picture{Name = "icon4.jpg" },
                new Picture{Name = "icon5.jpg" },
                new Picture{Name = "icon6.jpg" },
                new Picture{Name = "icon7.jpg" },
                new Picture{Name = "icon8.jpg" }
            };

        public static ObservableCollection<WorkPosition> Positions = new ObservableCollection<WorkPosition>()
            {
                new WorkPosition{ Name = "position 1" },
                new WorkPosition{ Name = "position 2" },
                new WorkPosition{ Name = "position 3" },
                new WorkPosition{ Name = "position 4" },
                new WorkPosition{ Name = "position 5" },
                new WorkPosition{ Name = "position 6" },
                new WorkPosition{ Name = "position 7" },
                new WorkPosition{ Name = "position 8" }
            };

        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new Strange_app.Main());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
